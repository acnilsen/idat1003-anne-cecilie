package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * Version 1.0
 * <p></p>
 * Represents the system time.
 * The class has the following attributes:
 * lastUpdatedTime
 * Created by Anne Cecile on 11.12.2023
 * <br> Finalized by Anne Cecile on 11.12.2023
 */

public class SystemTime {
  /**
   * The current system time. Works as a default value for the lastUpdatedTime variable.
   *
   * <br> The time is updated in the updateTime method.
   */
  private static LocalTime lastUpdatedTime = LocalTime.parse("00:00");

  /**
   * Gets the current time of the system.
   *
   * @return The current time of the system.
   */

  public static LocalTime getCurrentTime() {
    return lastUpdatedTime;
  }

  /**
   * Updates the time of the system. The time must be in the format HH:MM.
   * The method checks if the input time is after the current time, and if so, updates the time.
   * If the input time is before the current time, an IllegalArgumentException is thrown.
   *
   * @param inputTime The time to update to.
   * @return The new updated time.
   * @throws IllegalArgumentException if the input time is before the current time.
   * @throws IllegalArgumentException if the input time is not in the format HH:MM.
   *
   */

  public static LocalTime updateTime(String inputTime) {
    try {
      LocalTime newTime = LocalTime.parse(inputTime);

      if (newTime.isAfter(lastUpdatedTime)) {
        lastUpdatedTime = newTime;
      } else {
        throw new IllegalArgumentException("Input time is before current time"
            + " current time is:" + lastUpdatedTime);
      }
      return lastUpdatedTime; // Move this line inside the if block
    } catch (Exception e) {
      throw new IllegalArgumentException("Input time is not in correct format (HH:MM), "
          + "current time is:" + lastUpdatedTime);
    }
  }
}
