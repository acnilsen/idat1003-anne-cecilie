package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * Version 1.0
 * <p></p>
 * Object class for train departures.
 * The class has the following attributes:
 * departureTime, line, trainNumber, destination, delay, track
 * <p></p>The JavaDoc comments and get-methods in this clas are written
 * with assistance from GitHub CoPilot
 * <p></p>Created by Anne Cecile on 05.11.2023
 * <br> Finalized by Anne Cecile on 11.12.2023
 */

public class TrainDeparture {
  private final LocalTime departureTime; //Departure time in format hh:mm
  private final String line; //Train line number
  private final String trainNumber; //Train number
  private final String destination; //Destination of the train
  private int delay; //Delay in minutes
  private int track; //Track number

  /**
   * Constructor for TrainDeparture class.
   *
   * @param departureTime   Departure time in format hh:mm
   * @param line            Train line number
   * @param trainNumber     Train number
   * @param destination     Destination of the train
   * @param delay           Delay in minutes
   * @param track           Track number
   *
   */

  public TrainDeparture(LocalTime departureTime, String line, String trainNumber,
                        String destination, int delay, int track) {
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.delay = delay;
    this.track = track;
  }

  /**
   * Gets the departure time.
   *
   * @return The departure time in hh:mm format
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Gets the train line number.
   *
   * @return The train line number.
   */

  public String getLine() {
    return line;
  }

  /**
   * Gets the train number of the train.
   *
   * @return The train number of the train.
   */

  public String getTrainNumber() {
    return trainNumber;
  }

  /**
   * Gets the destination of the train.
   *
   * @return The destination of the train.
   */

  public String getDestination() {
    return destination;
  }

  /**
   * * Gets the delay of the train.
   *
   * @return The delay of the train.
   */

  public int getDelay() {
    return delay;
  }

  /**
   * Gets the track number of the train.
   *
   * @return The track number of the train.
   */

  public int getTrack() {
    return track;
  }

  /**
   * Sets the delay of the train.
   *
   * @param delay The delay of the train.
   */

  public void setDelay(int delay) {
    this.delay = delay;
  }

  /**
   * Sets the track number of the train.
   *
   * @param track The track number of the train.
   */

  public void setTrack(int track) {
    this.track = track;
  }

  /**
   * Returns a string representation of the object.
   * Only prints the new departure time if the delay is not 0.
   * Only prints the track number if the track number is not 0.
   *
   * @return A string representation of the object.
   */

  @Override
  public String toString() {
    LocalTime delayTime = departureTime.plusMinutes(delay);
    if (delay == 0) {
      if (track == 0) {
        return (departureTime + " " + line + " " + trainNumber + " " + destination);
      } else {
        return (departureTime + " " + line + " " + trainNumber + " " + destination + " " + track);
      }
    } else {
      if (track == 0) {
        return (departureTime + " New departure time: " + delayTime + " " + line + " "
            + trainNumber + " " + destination);
      } else {
        return (departureTime + " New departure time: " + delayTime + " " + line + " "
          + trainNumber + " " + destination + " " + track);
      }
    }
  }
}


