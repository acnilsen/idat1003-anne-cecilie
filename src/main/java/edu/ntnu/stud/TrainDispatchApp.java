package edu.ntnu.stud;

/**
 * Starts the user interface for managing train departures.
 * Created by Anne Cecile on 01.12.2023
 */

public class TrainDispatchApp {

  /**
   * Main method for the user interface.
   * Creates a TrainDispatchApp object and calls the startUi method.
   *
   * @param args Command line arguments.
   */

  public static void main(String[] args) {
    TrainDispatchApp trainDispatchApp = new TrainDispatchApp();
    trainDispatchApp.startUi();
  }

  /**
   * Starts the user interface. Creates a UserInterface object and
   * calls the init and start methods.
   * <br> The init method creates a TrainIndex object and adds some train departures.
   * <br> The start method starts the user interface.
   *
   * @see UserInterface
   */

  private void startUi() {
    UserInterface ui = new UserInterface();
    ui.init();
    ui.start();
  }
}

