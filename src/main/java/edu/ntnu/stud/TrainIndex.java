package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


/**
 * Version 1.0
 * <p></p>
 * This class represents a train index.
 * The class has the following methods:
 * addTrainDeparture, searchTrainNumber, searchTrainDestination, removeTrainDeparture,
 * printAllTrainDeparturesSorted, addDelay, changeTrack, updateTime
 *
 * <p></p>Created by Anne Cecile on 05.11.2023
 * <br> Finalized by Anne Cecile on 11.12.2023
 */

public class TrainIndex {
  /**
   * The list of train departures.
   * <br> The list is sorted by departure time in the addTrainDeparture method.
   */

  private final ArrayList<TrainDeparture> trainDepartures = new ArrayList<>();

  /**
   * Sorts the train departures by departure time.
   * Uses the sort method in the Collections class.
   */

  private void sortTrainDepartures() {
    trainDepartures.sort(Comparator.comparing(TrainDeparture::getDepartureTime));
  }

  /**
   * Constructor for TrainIndex class.
   * <br>Creates an empty list of train departures.
   */



  //public TrainIndex() {
  //}

  /**
   * Adds a train departure to the train index. The train number must be unique.
   * Uses the trainNumberExists method to check if the train number already exists.
   * After adding the train departure, the list is sorted by departure time.
   *
   * @param trainDeparture The train departure to add.
   *                       The train number must be unique.
   * @throws IllegalArgumentException if the train number already exists in the train index.
   */
  public void addTrainDeparture(TrainDeparture trainDeparture) {
    if (trainNumberExists(trainDeparture.getTrainNumber())) {
      throw new IllegalArgumentException("Train number already exists");
    }
    //Add the train departure to the list
    trainDepartures.add(trainDeparture);
    //Sort the list by departure time
    sortTrainDepartures();
  }

  /**
   * Checks if a train number already exists in the train index.
   * <br> Returns true if the train number already exists, false otherwise.
   *
   * @param trainNumber The train number to check.
   * @return True if the train number already exists, false otherwise.
   */

  public boolean trainNumberExists(String trainNumber) {
    for (TrainDeparture existingDeparture : trainDepartures) {
      //Check if the train number already exists in the train index
      if (existingDeparture.getTrainNumber().equals(trainNumber)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Searches for a train departure with the given train number.
   * <br> The method returns null if no train departures are found.
   *
   * @param trainNumber The train number to search for.
   * @return The train departure with the given train number, or null if not found.
   */
  public TrainDeparture searchTrainNumber(String trainNumber) {
    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getTrainNumber().equals(trainNumber)) {
        return trainDeparture;
      }
    }
    return null; //return null if not found
  }

  /**
   * Searches for a train departure with the given destination.
   * Ignores case when searching.
   * <br> Throws IllegalArgumentException if no train departures are found.
   *
   * @param destinationToFind The destination to search for.
   * @return The train departure with the given destination, or null if not found.
   * @throws IllegalArgumentException if no train departures are found.
   */
  public ArrayList<TrainDeparture> searchTrainDestination(String destinationToFind) {
    ArrayList<TrainDeparture> matchingDepartures = new ArrayList<>();

    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getDestination().equalsIgnoreCase(destinationToFind)) {
        matchingDepartures.add(trainDeparture);
      }
    }

    if (matchingDepartures.isEmpty()) {
      throw new IllegalArgumentException("Destination not found");
    }

    return matchingDepartures;
  }


  /**
   * Removes a train departure that has an earlier departure time than the curren system time.
   * Checks if the train departure has a delay, and if so, adds the delay to the departure time
   * before comparing. This makes sure that train departures are not removed before they have left,
   * but still ensures that train departures with a delay are removed.
   *
   * @param lastUpdatedTime The current system time.
   */

  public void removeEarlierTrainDeparture(LocalTime lastUpdatedTime) {
    Iterator<TrainDeparture> iterator = trainDepartures.iterator();

    while (iterator.hasNext()) {
      TrainDeparture trainDeparture = iterator.next();
      if (trainDeparture.getDelay() != 0) {
        LocalTime departureTimeRemove = trainDeparture.getDepartureTime()
            .plusMinutes(trainDeparture.getDelay());
        if (!departureTimeRemove.isAfter(lastUpdatedTime)) {
          iterator.remove();
        }
      } else if (!trainDeparture.getDepartureTime().isAfter(lastUpdatedTime)) {
        iterator.remove();
      }
    }
  }

  /**
   * Prints all train departures sorted by departure time.
   * The Arraylist trainDepartures is already sorted by departure time in the
   * addTrainDeparture method, so this method only needs to print the list.
   * Removes all train departures with an earlier departure time than the current system time.
   * <br>
   *
   * @return A string with all train departures sorted by departure time.
   * @throws IllegalArgumentException if no train departures are found.
   */
  public String printAllTrainDepartures() {
    removeEarlierTrainDeparture(SystemTime.getCurrentTime());

    if (trainDepartures.isEmpty()) {
      throw new IllegalArgumentException("No train departures found");
    }

    StringBuilder result = new StringBuilder();
    for (TrainDeparture trainDeparture : trainDepartures) {
      result.append(trainDeparture.toString()).append("\n");
    }
    return result.toString();
  }

  /**
   * Adds a delay to a train departure with the given train number.
   * Uses searchTrainNumber to find the train departure.
   * Then sets the delay using the setDelay method in the TrainDeparture class,
   * if the train number is found. <br> If the train number is not found,
   * an IllegalArgumentException
   * is thrown.
   *
   *
   * @param trainNumber The train number to add delay to.
   * @param delay       The delay to add in minutes.
   * @throws IllegalArgumentException if the train number is not found.
   */

  public void addDelay(String trainNumber, int delay) {
    TrainDeparture foundDepartureAddDelay = searchTrainNumber(trainNumber);

    if (foundDepartureAddDelay != null) {
      foundDepartureAddDelay.setDelay(delay);
    } else {
      throw new IllegalArgumentException("Train number not found");
    }
  }

  /**
   * Changes the track of a train departure with the given train number.
   * Uses searchTrainNumber to find the train departure. Then sets the track using the setTrack
   * method in the TrainDeparture class, if the train number is found.
   * <br> If the train number is not found, an IllegalArgumentException is thrown.
   * <br>
   *
   * @param trainNumber The train number to change track for.
   * @param track       The track to change to.
   * @throws IllegalArgumentException if the train number is not found.
   */

  public void changeTrack(String trainNumber, int track) {
    TrainDeparture foundDepartureChangeTrack = searchTrainNumber(trainNumber);

    if (foundDepartureChangeTrack != null) {
      foundDepartureChangeTrack.setTrack(track);
    } else {
      throw new IllegalArgumentException("Train number not found");
    }
  }




}