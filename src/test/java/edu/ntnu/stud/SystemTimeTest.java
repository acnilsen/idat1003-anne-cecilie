package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SystemTimeTest {

  @Test
  @DisplayName("Test that getCurrentTime returns the current time")
  void getCurrentTime() {
    // Arrange
    SystemTime.updateTime("12:01");
    // Act
    LocalTime currentTime = SystemTime.getCurrentTime();
    // Assert
    assertEquals(LocalTime.parse("12:01"), currentTime);
  }

  @Test
  @DisplayName("Test that updateTime updates the time of the system")
  void updateTimeTest() {
    // Arrange
    // Act
    SystemTime.updateTime("12:00");
    LocalTime updateTimeResult = SystemTime.getCurrentTime();
    // Assert
    assertEquals(LocalTime.parse("12:00"), updateTimeResult);
  }
}
