package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Test-class for the TrainDeparture class.
 * The tests in this class are written with assistance from GitHub Copilot.
 */

class TrainDepartureTest {
  @Test
  @DisplayName("Test that getDepartureTime returns the departure time")
  void getDepartureTime() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 0, 1);
    //Act
    LocalTime departureTimeResult = trainDeparture.getDepartureTime();
    //Assert
    assertEquals(LocalTime.parse("12:00"), departureTimeResult);
  }

  @Test
  @DisplayName("Test that getLine returns the line")
  void getLine() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 0, 1);
    //Act
    String lineResult = trainDeparture.getLine();
    //Assert
    assertEquals("L1", lineResult);
  }

  @Test
  @DisplayName("Test that getTrainNumber returns the train number")
  void getTrainNumber() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 0, 1);
    //Act
    String trainNumberResult = trainDeparture.getTrainNumber();
    //Assert
    assertEquals("123", trainNumberResult);
  }

  @Test
  @DisplayName("Test that getDestination returns the destination")
  void getDestination() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
            "L1", "123", "Oslo", 0, 1);
    //Act
    String destinationResult = trainDeparture.getDestination();
    //Assert
    assertEquals("Oslo", destinationResult);
  }

  @Test
  @DisplayName("Test that getDelay returns the delay")
  void getDelay() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
                "L1", "123", "Oslo", 0, 1);
    //Act
    int delayResult = trainDeparture.getDelay();
    //Assert
    assertEquals(0, delayResult);
  }

  @Test
  @DisplayName("Test that getTrack returns the track")
  void getTrack() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
                        "L1", "123", "Oslo", 0, 1);
    //Act
    int trackResult = trainDeparture.getTrack();
    //Assert
    assertEquals(1, trackResult);
  }

  @Test
  @DisplayName("Test that setDelay sets the delay")
  void setDelay() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 0, 1);
    //Act
    trainDeparture.setDelay(10);
    int setDelayResult = trainDeparture.getDelay();
    //Assert
    assertEquals(10, setDelayResult);
  }

  @Test
  @DisplayName("Test that setTrack sets the track")
  void setTrack() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 0, 1);
    //Act
    trainDeparture.setTrack(2);
    int setTrackResult = trainDeparture.getTrack();
    //Assert
    assertEquals(2, setTrackResult);
  }

  @Test
  @DisplayName("Test that toString returns the correct string")
  void toStringTestTrack() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 0, 1);
    //Act
    String toStringTrackResult = trainDeparture.toString();
    //Assert
    assertEquals("12:00 L1 123 Oslo 1", toStringTrackResult);
  }

  @Test
  @DisplayName("Test that toString returns the correct string when delay is not 0")
  void toStringTestDelay() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 10, 0);
    //Act
    String toStringDelayResult = trainDeparture.toString();
    //Assert
    assertEquals("12:00 New departure time: 12:10 L1 123 Oslo", toStringDelayResult);
  }

  @Test
  @DisplayName("Test that toString returns the correct string with delay and track")
  void toStringTestDelayAndTrack() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 10, 2);
    //Act
    String toStringDelayAndTrackResult = trainDeparture.toString();
    //Assert
    assertEquals("12:00 New departure time: 12:10 L1 123 Oslo 2",
        toStringDelayAndTrackResult);
  }

  @Test
  @DisplayName("Test that toString returns the correct string without delay and track")
  void toStringTestNoDelayAndTrack() {
    //Arrange
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 0, 0);
    //Act
    String toStringNoDelayAndTrackResult = trainDeparture.toString();
    //Assert
    assertEquals("12:00 L1 123 Oslo", toStringNoDelayAndTrackResult);
  }

}

