package edu.ntnu.stud;

import static java.lang.System.out;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Test-class for the TrainIndex class.
 * The tests in this class are written with assistance from GitHub Copilot.
 */

class TrainIndexTest {
  @Test
  @DisplayName("Test that addTrainDeparture adds a train departure to the train index")
  void addTrainDeparture() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"), "L1",
        "123", "Oslo", 0, 1);
    // Act
    trainIndex.addTrainDeparture(trainDeparture);
    TrainDeparture addTraindepartureResult = trainIndex.searchTrainNumber("123");
    // Assert
    assertEquals(trainDeparture, addTraindepartureResult);
  }

  @Test
  @DisplayName("Test that addTrainDeparture throws IllegalArgumentException if the train number "
      + "already exists in the train index")
  void addTrainDepartureThrowsIllegalArgumentException() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"), "L1",
        "123", "Oslo", 0, 1);
    out.println(trainDeparture);
    // Act
    trainIndex.addTrainDeparture(trainDeparture);

    // Assert
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
        trainIndex.addTrainDeparture(trainDeparture));
    assertEquals("Train number already exists", e.getMessage());
  }

  @Test
  @DisplayName("Test that trainNumberExists returns true if the train number exists")
  void trainNumberExists() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"), "L1",
        "123", "Oslo", 0, 1);
    // Act
    trainIndex.addTrainDeparture(trainDeparture);
    // Assert
    assertTrue(trainIndex.trainNumberExists("123"));
  }

  @Test
  @DisplayName("Test that trainNumberExists returns false if the train number does not exist")
  void trainNumberExistsReturnsFalse() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"), "L1",
        "123", "Oslo", 0, 1);
    // Act
    trainIndex.addTrainDeparture(trainDeparture);
    // Assert
    assertFalse(trainIndex.trainNumberExists("137"));
  }

  @Test
  @DisplayName("Test that searchTrainNumber returns the train departure with "
      + "the given train number")
  void searchTrainNumber() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"),
        "L1", "123", "Oslo", 0, 1);
    // Act
    trainIndex.addTrainDeparture(trainDeparture);
    TrainDeparture searchTrainNumberResult = trainIndex.searchTrainNumber("123");
    // Assert
    assertSame(trainDeparture, searchTrainNumberResult);
  }


  @Test
  @DisplayName("Test that searchTrainNumber returns null if the train number is not found")
  void searchTrainNumberReturnsNull() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"), "L1",
        "123", "Oslo", 0, 1);
    trainIndex.addTrainDeparture(trainDeparture);
    // Act
    TrainDeparture searchTrainNumberResultNegative = trainIndex.searchTrainNumber("465");
    // Assert
    assertNull(searchTrainNumberResultNegative);
  }

  @Test
  @DisplayName("Test that searchTrainDestination returns the train departure with the given "
      + "destination")
  void searchTrainDestination() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"), "L1",
          "123", "Oslo", 0, 1);
    trainIndex.addTrainDeparture(trainDeparture);
    // Act
    List<TrainDeparture> searchTrainDestinationResult = trainIndex.searchTrainDestination("Oslo");
    // Assert
    assertTrue(searchTrainDestinationResult.contains(trainDeparture));
  }

  @Test
  @DisplayName("Test that searchTrainDestination throws IllegalArgumentException if the "
      + "destination is not found")
  void searchTrainDestinationThrowsIllegalArgumentException() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.parse("12:00"), "L1",
        "123", "Oslo", 0, 1);
    trainIndex.addTrainDeparture(trainDeparture);
    // Act

    // Assert
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
        trainIndex.searchTrainDestination("Trondheim"));
    assertEquals("Destination not found", e.getMessage());

  }


  @Test
  @DisplayName("Test that removeTrainDeparture removes a train departure with the given or"
      + " earlier departure time")
  void removeEarlierTrainDeparture() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture1 = new TrainDeparture(LocalTime.parse("12:00"), "L1",
        "123", "Oslo", 0, 1);
    TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.parse("12:30"), "L1",
          "456", "Oslo", 0, 1);
    trainIndex.addTrainDeparture(trainDeparture1);
    trainIndex.addTrainDeparture(trainDeparture2);
    // Act
    trainIndex.removeEarlierTrainDeparture(LocalTime.parse("12:15"));
    // Assert
    assertNull(trainIndex.searchTrainNumber("123"));
    assertNotNull(trainIndex.searchTrainNumber("456"));
  }

  @Test
  @DisplayName("Test that printTrainDepartures prints all train departures")
  void printTrainDepartures() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture1 = new TrainDeparture(LocalTime.parse("14:00"), "L1",
        "123", "Oslo", 0, 1);
    TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.parse("12:30"), "L1",
        "456", "Oslo", 0, 1);
    trainIndex.addTrainDeparture(trainDeparture1);
    trainIndex.addTrainDeparture(trainDeparture2);
    // Act
    trainIndex.printAllTrainDepartures();
    // Assert
    assertTrue(trainIndex.printAllTrainDepartures().contains(trainDeparture1.toString()));
    assertTrue(trainIndex.printAllTrainDepartures().contains(trainDeparture2.toString()));

  }

  @Test
  @DisplayName("Test that printTrainDepartures prints all train departures, negative")
  void printTrainDeparturesNegative() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    // Act

    // Assert
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
        trainIndex.printAllTrainDepartures());
    assertEquals("No train departures found", e.getMessage());
  }

  @Test
  @DisplayName("Test that addDelay adds a delay to a train departure")
  void addDelayTest() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture1 = new TrainDeparture(LocalTime.parse("12:00"), "L1",
        "123", "Oslo", 0, 1);
    trainIndex.addTrainDeparture(trainDeparture1);
    // Act
    trainIndex.addDelay("123", 10);
    int addDelayResult = trainDeparture1.getDelay();
    // Assert
    assertEquals(10, addDelayResult);
  }

  @Test
  @DisplayName("Test that addDelay throws IllegalArgumentException if the "
      + "train number is not found")
  void addDelayThrowsIllegalArgumentException() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    // Act

    // Assert
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
        trainIndex.addDelay("123", 10));
    assertEquals("Train number not found", e.getMessage());
  }

  @Test
  @DisplayName("Test that changeTrack changes the track of a train departure")
  void changeTrackTest() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    TrainDeparture trainDeparture1 = new TrainDeparture(LocalTime.parse("12:00"), "L1",
        "123", "Oslo", 0, 1);
    trainIndex.addTrainDeparture(trainDeparture1);
    // Act
    trainIndex.changeTrack("123", 2);
    int changeTrackResult = trainDeparture1.getTrack();
    // Assert
    assertEquals(2, changeTrackResult);
  }

  @Test
  @DisplayName("Test that changeTrack throws IllegalArgumentException if the "
      + "train number is not found")
  void changeTrackThrowsIllegalArgumentException() {
    // Arrange
    TrainIndex trainIndex = new TrainIndex();
    // Act

    // Assert
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
        trainIndex.changeTrack("123", 2));
    assertEquals("Train number not found", e.getMessage());
  }


}

