# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = Anne Cecilie Skår Nilsen  
STUDENT ID = 111812

## Project description
This project is a program that manage train departures from a station.
The program will be able to add new departures, print out all departures in a sorted list, 
and search for specific departures. The user can also change track or delay for an existing departure,
or choose to exit the program.

## Project structure

The project structure is as follows:
The classes are in the main folder, and the test classes are in the test folder.

## Link to repository
https://gitlab.stud.idi.ntnu.no/acnilsen/idat1003-anne-cecilie

## How to run the project
The project is run from TrainDispatchApp, it makes an instance of UserInterface, which starts the menu.
The menu is a while loop that runs until the user chooses to exit the program.

## How to run the tests
The tests are run from the test folder, and the test classes are named after the class they are testing.
